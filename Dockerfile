FROM jboss/wildfly:18.0.1.Final

ARG WAR_FILE

COPY build ./
COPY build /tmp

USER root
RUN chmod 755 ./scripts/wildfly.sh

USER jboss
RUN ./scripts/wildfly.sh

USER root
RUN chgrp -R 0 $JBOSS_HOME && chmod -R g=u $JBOSS_HOME
RUN rm -rf /opt/jboss/wildfly/standalone/configuration/standalone_xml_history/current

USER jboss


LABEL maintainer="Max Weis"
LABEL email="weis98max@gmail.com"
LABEL image.title="wildfy-pq"
LABEL image.description="JBoss Wildfly container image with a PostgreSQL datasource"
LABEL image.vcs-url="https://gitlab.com/max-develop/wildfy-pq"