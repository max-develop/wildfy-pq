# Wildfly Container Image with Postgres Datasource

This Wildfly container images containes a PostgresSQL Datasource

## Credentials

|||
|---|---|
|DB Name|`postgres`|
|DB User|`postgres`|
|DB Password|`postgres`|

Path to postgres instance `postgresql:5432`